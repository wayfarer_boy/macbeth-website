#!/bin/bash

FFMPEG="$HOME/bin/ffmpeg"
INFILE="$1"
OUTFILE="$( basename "$INFILE" )"
OUTFILE="${OUTFILE%.*}.%s"
OUTWEBM="$( printf $OUTFILE webm)"
OUTMP4="$( printf $OUTFILE mp4)"
OUTOGG="$( printf $OUTFILE ogv)"
SCALE="720:406"

echo -n "Converting $INFILE to webm..."
# WebM conversion
rm -f "$OUTWEBM"
$FFMPEG -y -i "$INFILE" -strict experimental -f webm -filter:v scale=$SCALE -vcodec libvpx -g 120 -lag-in-frames 16 -deadline good -cpu-used 0 -vprofile 0 -qmax 53 -qmin 0 -b:v 768k -acodec libvorbis -ab 112k -ar 44100 $OUTWEBM

echo -n " mp4..."
# MP4 conversion
rm -f "$OUTMP4"
$FFMPEG -y -i "$INFILE" -strict experimental -filter:v scale=$SCALE -acodec aac -ab 96k -vcodec libx264 -preset slow -f mp4 -crf 22 $OUTMP4

echo -n " ogg..."
# OGG conversion
rm -f "$OUTOGG"
$FFMPEG -y -i "$INFILE" -filter:v scale=$SCALE -codec:v libtheora -qscale:v 5 -codec:a libvorbis -qscale:a 3 $OUTOGG

echo " done"

exit 0
