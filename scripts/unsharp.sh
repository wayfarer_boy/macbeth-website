#!/bin/bash

UNSHARP="5.0x1+0.5+0"
for i in $1
do
  convert $i -resize 624x -unsharp $UNSHARP -unsharp $UNSHARP -unsharp $UNSHARP -unsharp $UNSHARP /tmp/tmp.jpg
  jpegtran -optimize /tmp/tmp.jpg > ../images/display/`basename $i .png`.jpg
done

exit 0
