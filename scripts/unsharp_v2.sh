#!/bin/bash

convert "$1" -resize 1248x240 -modulate 100,80 -level ,95% -unsharp '5.0x1+0.5+0' -unsharp '5.0x1+0.5+0' /tmp/tmp.jpg
jpegtran -optimize /tmp/tmp.jpg > "$1"

exit 0
