#!/bin/bash

convert "$1" -resize 200x200 -modulate 100,60 -level ,80% -unsharp '5.0x1+0.5+0' -unsharp '5.0x1+0.5+0' /tmp/tmp.jpg
jpegtran -optimize /tmp/tmp.jpg > "$1"

exit 0
