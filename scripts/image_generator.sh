#!/bin/bash

function GenerateImage {
  VAL1=$((RANDOM % 100))
  VAL2=$((RANDOM % 100))
  VAL3=$((RANDOM % 100))
  SIZE="$1x$2"
  X=$1
  Y=$2
  XHALF=$((X/2))
  YHALF=$((Y/2))
  convert -size $SIZE plasma:fractal -blur 0x$((RANDOM % 10)) -crop ${XHALF}x${YHALF}+$((RANDOM % XHALF))+$((RANDOM % YHALF)) +repage -resize $1 -modulate $((VAL1+50)),$((VAL2+50)),$((VAL3+50)) /tmp/tmp.jpg
}

# Generate placeholder images
# for use on the Macbeth site

GENERATE=0
BASE="/home/alpagan/Websites/static/static/macbeth"
NAMES="macbeth macduff lady malcolm banquo"
TYPES="%s_s%d.jpg %s_s%d_a1.jpg %s_s%d_a2.jpg %s_s%d_a3.jpg"
SIZES="624x120|images/display"
SIZES="$SIZES 150x84|images/thumbs"
SIZES="$SIZES 100x100|images/profiles"
#SIZES="$SIZES 624x120|images/display"
#SIZES="$SIZES 200x300|images/profiles_large"

for SIZE in $SIZES
do
  DIR=$( echo $SIZE | cut -d '|' -f 2 )
  echo "Emptying $DIR..."
  mkdir -p $BASE/$DIR
  rm -f $BASE/$DIR/*
done

echo
if [ $GENERATE -eq 1 ]
then
  echo "Creating images for character placeholder"
  GenerateImage 100 100
  jpegtran -optimize -outfile $BASE/images/profiles/character.jpg /tmp/tmp.jpg
  echo "Creating images for intro placeholder"
  GenerateImage 100 100
  jpegtran -optimize -outfile $BASE/images/display/intro.jpg /tmp/tmp.jpg
  echo "Creating images for outro placeholder"
  GenerateImage 100 100
  jpegtran -optimize -outfile $BASE/images/display/outro.jpg /tmp/tmp.jpg
fi

for NAME in $NAMES
do
  echo "Creating images for $NAME"
  for SIZE in $SIZES
  do
    RESIZE=$( echo $SIZE | cut -d '|' -f 1 )
    DIR=$( echo $SIZE | cut -d '|' -f 2 )
    echo -n "size: $RESIZE, dir: $DIR "
    for T in $TYPES
    do
      for NUM in `seq 4`
      do
        if [ $NUM -eq 4 ]
        then
          INFILE="$( printf "$BASE/images/display/%s.jpg" $NAME )"
          OUTFILE="$( printf "$BASE/$DIR/%s.jpg" $NAME )"
        else
          INFILE="$( printf "$BASE/images/display/$T" $NAME $NUM )"
          OUTFILE="$( printf "$BASE/$DIR/$T" $NAME $NUM )"
        fi
        if [ "$DIR$GENERATE" == "images/display1" ]
        then
          X=$( echo $RESIZE | cut -d 'x' -f 1 )
          Y=$( echo $RESIZE | cut -d 'x' -f 2 )
          GenerateImage $X $Y
        else
          convert $INFILE -resize $RESIZE^ -gravity center -crop $RESIZE+0+0 /tmp/tmp.jpg
        fi
        jpegtran -optimize -outfile $OUTFILE /tmp/tmp.jpg
        echo -n '.'
      done
    done
    echo " done"
  done
  echo
done

exit 0
