/*
 * List of images required:

    video/intro.jpg
    video/outro.jpg
    video/macbeth_s#.jpg              624 x 352

    images/thumbs/macbeth.jpg          20 x  20

    images/profiles/macbeth.jpg
    images/profiles/character.jpg     100 x 100  Silhouette character image

    images/display/macbeth_s#_a#.jpg  624 x 120

*/

var macbeth;
macbeth = (function ($, soundManager, window, document, undefined) {

  var m = window.macbeth;

  m = {
  
    settings: {},
    audio: {
      sounds: [ 'dagger', 'cards', 'raven', 'witches_1', 'witches_2', 'witches_3' ],
      vols: [ 50, 50, 30, 30, 30, 30 ],
      start: 2,
      total: 4,
      min: 5000,
      max: 10000,
      tmout: null,
      mute: true
    },
    video: {
      defOptions: {
        features: []
      }
    },
    selected: {
      character: null,
      scene: null,
      segment: -1
    },
    templates: {},

    init: function () {
      m.initSettings(function () {
        m.body = $('body');
        m.main = $('#main').verticalCentre();
        m.loading = $('#loading');
        m.splash = $('#splash');
        m.compass = $('#compass');
        m.credits = $('#credits', m.main);
        m.intro = $('#intro', m.main);
        m.outro = $('#outro', m.main);
        m.segments = $('#segments', m.main);
        m.characters = $('#characters', m.main);
        m.scenes = $('#scenes', m.main);
        m.progress = $('#progress', m.main);
        m.overview = $('#overview', m.main);
        m.messages = $('#messages');
        m.focus = $('#main > div,header,#compass');
        m.templates.activity = Handlebars.compile($('#activity-template').html());
        m.templates.overview = Handlebars.compile($('#overview-template').html());
        m.templates.scenes = Handlebars.compile($('#scenes-template').html());
        m.templates.characters = Handlebars.compile($('#characters-template').html());
        m.imgList = [m.settings.directories['profiles']+'character.jpg'];
        m.initLoading();
        m.initSplash();
        m.initSoundManager(m.initAudio);
        m.initCookie();
        m.initDom();
        m.initInteractivity();
      });
    },

    initDom: function() {
      m.initIntroOutro(m.intro, 'intro');
      m.initIntroOutro(m.outro, 'outro');
      m.initOverview();
      m.initCharacters();
      m.initProgress();
    },

    initInteractivity: function () {
      m.droppableCompass();
      m.overview.clickOverview();
      m.characters.clickCharacters();
      m.scenes.clickScenes();
      m.preloadImages(m.finishMessages);
    },
    
    // Setup the loading screen (uses canvasloader instance)
    initLoading: function () {
      var cl = new CanvasLoader(m.loading.attr('id'));
      cl.setColor('#ffffff'); // default is '#000000'
      cl.setShape('spiral'); // default is 'oval'
      cl.setDiameter(86); // default is 40
      cl.setDensity(77); // default is 40
      cl.setRange(1); // default is 1.3
      cl.setSpeed(3); // default is 2
      cl.setFPS(50); // default is 24
      cl.show(); // Hidden by default 
      m.loadingCl = cl;
    },

    initSplash: function () {
      $('a', m.splash).click(function (e) {
        if (m.splash.hasClass('inactive')) e.preventDefault();
      });
    },

    initSettings: function (callback) {
      $.ajax({
        url: './js/settings.json',
        success: function(data, textStatus, jqXHR) {
          m.settings = data;
          callback();
        }
      });
    },

    initSoundManager: function (callback) {
      soundManager.setup({
        useAltURL: (!document.location.protocol.match(/http/i)), 
        url: 'js/soundmanager/swf/',
        altURL: './soundmanager/swf/',
        debug: false,
        onready: function() {
          soundManager.createSound({
            id: 'ambience', // required
            url: 'audio/ambience.mp3', // required
            volume: 100,
            autoPlay: true,
            autoLoad: true,
            stream: true,
            loops: 1440, // 24 hours' worth of playback
            onload: callback
          });
        }
      });
    },

    // Setup audio files for use with SoundManager
    initAudio: function () {
      if (soundManager.getSoundById('ambience') != undefined) {
        for (var i = 0; i < m.audio.sounds.length; i+=1) {
          soundManager.createSound({
            id: m.audio.sounds[i], // required
            url: 'audio/'+m.audio.sounds[i]+'.mp3', // required
            // optional sound parameters here, see Sound Properties for full list
            volume: m.audio.vols[i],
            autoPlay: false,
            autoLoad: true
          });
        }
      }
    },

    initCookie: function () {
      // Setup cookie (create if not available)
      $.cookie.json = true;
      if ($.cookie('macbeth') == null) {
        $.cookie('macbeth', '{}');
        m.body.data('cookie', {});
      }
      m.loadingMessage('Checking cookie');
      // Create data variables (cookie: game progress, settings: site settings)
      m.cookie = $.parseJSON($.cookie('macbeth'));
    },

    initIntroOutro: function (ele, type) {
      $('a.begin', ele).click(function () {
        m.loadingActive(true);
        m.startAmbience();
        setTimeout(function() {
          m[type].setFocus({ c: m[type], focus: false, hover: false });
          $('video', m.intro)[0].pause();
        }, 1000);
        m.loadingActive(false);
      });
    },

    introOutroVideo: function (ele) {
      var video = $('video', ele),
          button = $('a.begin', ele);
      m.stopAmbience();
      video.mediaelementplayer(m.video.defOptions);
      video.on('play', function () {
        button.removeClass('active');
      }).on('pause ended', function () {
        button.addClass('active');
      });
    },

    initOverview: function () {
      m.loadingMessage('Setting up overview cards');
      var count = 1;
      $.each(m.settings.characters, function(key, val) {
        if (key !== '_comment') {
          var arr = { 
            id: count,
            name: key,
            title: val.title,
            subtitle: val.description,
            scenes: [],
            complete: 'complete'
          };
          arr = m.initCharacterCard(arr, count, val, key)
          m.overview.prepend(m.templates.overview(arr));
          count += 1;
        }
      });
    },

    initCharacterCard: function (arr, count, val, key) {
      var COMPLETE = true, c = '', adj = '';
      $.each(val.scenes, function(k, v) {
        try {
          adj = m.settings.compass[m.cookie[key][v.scene]].adjective;
          if (adj) {
            c = 'complete';
          } else {
            COMPLETE = false;
          }
        } catch(e) {
          COMPLETE = false;
        }
        arr.scenes.push({
          id: key+'-'+v.scene,
          complete: c,
          title: m.settings.scenes[v.scene].title,
          video: m.settings.scenes[v.scene].video,
          adj: adj
        });
      });
      if (!COMPLETE) arr.complete = '';
      return arr;
    },

    initCharacters: function () {
      var charArr = { characters: [] };;
      m.loadingMessage('Compiling lists of characters and scenes');
      $.each(m.settings.characters, function(key, val) {
        if (key != '_comment') {
          var scenes, sceneArr = { name: key, scenes: [] };
          sceneArr = m.initCharacterScenes(sceneArr, val.scenes, key);
          m.scenes.append(m.templates.scenes(sceneArr));
          scenes = $('#scenes-'+key+' .scene', m.scenes)
            .click(function() { $(this).sceneSelect(); });
          charArr.characters.push({
            name: key,
            title: val.title,
            subtitle: val.description,
            complete: scenes.filter('.complete').length === 3 ? 'complete' : ''
          });
        }
      });
      m.characters.append(m.templates.characters(charArr));
      $('.character', m.characters)
        .click(function() { $(this).characterSelect($(this).attr('data-key')); });
    },

    initCharacterScenes: function (arr, scenes, character) {
      var i = 1, complete = '', v;
      $.each(scenes, function(key, val) {
        try {
          v = m.cookie[character][val.scene];
        } catch (e) {
          v = '';
        }
        if (m.settings.segments.indexOf(v) == -1) v = m.cookieVal(character, val.scene, '');
        arr.scenes.push({
          key: character+'_'+i,
          name: character,
          title: m.settings.scenes[val.scene].title,
          count: i,
          complete: !v ? '': 'complete'
        });
        i += 1;
      });
      return arr;
    },

    // Setup the reset button (hidden in the header)
    initProgress: function () {
      m.loadingMessage('Setting up progress area');
      $('.reset-question', m.progress)
        .click(function() {
          m.progress.addClass('confirm');
          m.progress.hover(function() {}, function() {
            m.progress.removeClass('confirm');
          });
        });
      $('.reset-confirm', m.progress)
        .click(function() {
          m.progress.removeClass('confirm').addClass('action');
          $.cookie('macbeth', '{}');
          setTimeout(function() {
            window.location = './';
          }, 500);
        });
    },

    // Setup compass centre positions and dimensions variables
    droppableCompass: function() {
      var compassPos = m.compass.offset();
      m.dims = {
        w: m.compass.width(),
        h: m.compass.height(),
        xMin: compassPos.left,
        yMin: compassPos.top
      };
      m.dims.xMax = m.dims.xMin + m.dims.w;
      m.dims.yMax = m.dims.yMin + m.dims.h;
      m.dims.cX = m.dims.xMin + m.dims.w * 0.5;
      m.dims.cY = m.dims.yMin + m.dims.h * 0.5;
    },

    preloadImages: function(callback) {
      var i, j;
      $.each(m.settings.characters, function(key, val) {
        if (key != '_comment') {
          m.imgList.push(m.settings.directories['profiles']+key+'.jpg');
          for (i = 1; i < 4; i+=1) {
            m.imgList.push(m.settings.directories['video']+key+'_s'+i+'.jpg');
            for (j = 1; j < 4; j+=1) {
              m.imgList.push(m.settings.directories['display']+key+'_s'+i+'_a'+j+'.jpg');
            }
          }
        }
      });
      $(m.imgList).preloadImages(callback);
    },

    loadingMessage: function (msg) {
      $('<p>').html(msg).appendTo(m.messages);
      var messages = $('p', m.messages);
      if (messages.length > 5) {
        m.loading.addClass('animating');
        messages.first().animate({
          marginTop: (messages.length - 6) * -10 - 5
        }, 10, function() {
          if (msg == 'Ready') m.loading.removeClass('animating');
        });
      }
    },

    loadingActive: function (active) {
      if (active) {
        m.loadingCl.show();
        m.loading.removeClass('inactive');
      } else {
        m.loading.addClass('inactive');
        setTimeout(function() {
          m.messages.empty();
          m.loadingCl.hide();
        }, 1000);
      }
    },

    // FadeIn and FadeOut for soundManager
    volFader: function (snd, dir, dur) {
      var maxVol = 90,
          start = maxVol,
          end = 0,
          soundObj = soundManager.getSoundById(snd);
      if (soundObj != undefined) {
        if (dir === 'in') {
          start = 0;
          end = maxVol;
          soundObj.play();
        }
        if (soundObj.playState) {
          jQuery({vol: start})
            .stop()
            .clearQueue()
            .animate({vol: end}, {
              duration: dur,
              step: function() { // called on every step
                // Update the element's text with rounded-up value:
                soundObj.setVolume(this.vol);
              }
            });
        }
      }
    },

    startAmbience: function () {
      if (m.audio.mute) {
        m.volFader('ambience', 'in', 2000);
        m.audio.mute = false;
        m.playRandomAudio();
      }
    },

    // Play random bits of audio
    playRandomAudio: function () {
      var sndName, dur, newVol;
      setTimeout(function() {
        sndName = m.audio.sounds[Math.floor(Math.random() * m.audio.total) + m.audio.start];
        if (sndName) {
          dur = 10000;
          if (!m.audio.mute) {
            newVol = 30;
            if (sndName === 'dagger' || sndName === 'cards') newVol = 50;
            sfx = soundManager.play(sndName, {
              volume: m.audio.vols[newVol] 
            });
            dur = sfx.duration;
          }
          setTimeout(function() {
            m.playRandomAudio();
          }, dur);
        } else {
          m.playRandomAudio();
        }
      }, Math.random() * m.audio.max + m.audio.min);
    },

    stopAmbience: function () {
      m.audio.mute = true;
      m.volFader('ambience', 'out', 4000);
      for (var i = 0; i < m.audio.sounds.length; i+=1) {
        m.volFader(m.audio.sounds[i], 'out', 2000);
      }
    },

    // Set a cookie value (or reset a value to '')
    // This function also updates the body data variable
    cookieVal: function (character, scene, val) {
      console.log(character, scene, val);
      v = val ? val : '';
      if (!m.cookie[character]) m.cookie[character] = {};
      m.cookie[character][scene] = v;
      $.cookie('macbeth', JSON.stringify(m.cookie));
    },

    // Finish all messages before loading up next part of site
    finishMessages: function () {
      var splash = $('#splash');
      m.loadingMessage('Ready');
      splash.removeClass('inactive');
      m.body.removeData('messages');
      m.resizeAll();
      $(window).resize(m.resizeAll);
      setTimeout(function() {
        m.loadingActive(false);
        m.body.addClass('ready');
        m.updateProgress();
        m.intro.setFocus({ c: m.intro, focus: true, hover: false });
        m.introOutroVideo(m.intro);
        m.loadingActive(false);
        splash.addClass('inactive');
        setTimeout(function () {
          m.intro.addClass('ready');
        }, 2000);
      }, 3000);
    },

    resizeAll: function () {
      $('.resize').each(function() {
        $(this).verticalCentre();
      });
      m.droppableCompass();
    },

    updateProgress: function () {
      var progress = 0,
          total = 0, perc, s;
      $.each(m.settings.characters, function(character, scenes) {
        if (character !== '_comment') {
          $.each(scenes.scenes, function(id, scene) {
            total++;
            s = m.cookie[character][scene.scene];
            if (s !== '') progress++;
          });
        }
      });
      if (progress == total) {
        m.progress.addClass('complete');
      } else {
        m.progress.removeClass('complete');
      }
      perc = Math.ceil(progress / total * 100);
      $('div.progressbar-value', m.progress).css({width: perc+'%'});
      $('h2 span', m.progress).text(perc);
    },

    updateOverview: function (options) {
      var adj = $('#'+options.character+'-'+options.scene+'-overview p'),
          card = adj.html(m.settings.compass[options.segment].adjective).parent(),
          item = card.parent().parent();
      if (options.add) {
        card.addClass('complete');
      } else {
        card.removeClass('complete');
      }
      if (item.find('div.sceneval-wrapper > div.complete').length == 3) {
        item.addClass('complete');
      } else {
        item.removeClass('complete');
      }
    },

    // Drag function monitors where user drags a scene
    // and uses trig to see if the scene is over a compass segment 
    drag: function (e) {
      e.preventDefault();
      var orig = $(e.target.parentNode), drag;
      if (!orig.hasClass('complete')) {
        m.scenes.unbind('hover')
          .setFocus({ c: m.scenes, focus: false, hover: true, nosound: true });
        drag = orig.createDragObj(m.body, e);
        $(window).mousemove(function(e) {
          drag.css({
            top: e.pageY - 28,
            left: e.pageX - 50
          });
          m.compass.highlightSector(e.pageX, e.pageY);
        });
        drag.on('mouseup', function(e) {
          var segment, oPos;
          $(window).unbind('mousemove');
          if (m.selected.segment >= -1) {
            m.sfx('dagger');
            segment = $('.segment-'+m.selected.segment, m.segments).addScene(drag);
            drag
              .unbind('mouseup')
              .animate({
                top: segment.offset().top + segment.height() * 0.5,
                left: segment.offset().left + segment.width() * 0.5
              }, 250)
              .find('img')
              .animate({
                width: 0,
                opacity: 0
              }, 250, function() {
                drag.remove();
              });
          } else {
            oPos = orig.offset();
            drag
              .unbind('mouseup')
              .animate({
                top: oPos.top,
                left: oPos.left
              }, 500)
              .find('img')
              .animate({
                width: orig.width(),
                opacity: 0
              }, 500, function() {
                drag.remove();
              });
            $('> .active', m.compass).removeClass('active');
          }
          $('> .active', m.compass).addClass('fade-out').removeClass('active fade-out');
        });
      }
    },

    sfx: function (id) {
      var sfx = soundManager.getSoundById(id);
      if (sfx) {
        sfx.setVolume(m.audio.vols[m.audio.sounds.indexOf(id)]).play();
      }
    },

    // Get the bearing from the centre of the compass to the current mouse location
    getBearing: function (x1, y1, x2, y2) {
      var y = y2 - y1,
          op = false;
      if (y < 0) {
        y = Math.abs(y2 - y1);
        op = true;
      }
      return m.toDeg(Math.atan2(y, x2 - x1), op);
    },


    // Convert radians to degreees and convert negative results to
    // clockwise/anticlockwise angles
    toDeg: function (rad, op) {
      var ret;
      if (op) {
        ret = 360 - (rad * 180 / Math.PI);
      } else {
        ret = rad * 180 / Math.PI;
      }
      return ret + 90;
    }

  };

  $(document).ready(function () {
    m.init();
  });
    
  // Centre elements vertically
  $.fn.verticalCentre = function() {
    var self = $(this),
        winH = Math.max(720, $(window).height()), minTop, h,
        par = self.parent();
    if (par.attr('id') == 'windowsize') winH = par.height();
    minTop = winH;
    return self
      .each(function() {
        h = $(this).height();
        minTop = Math.min((winH - h) * 0.5, minTop);
      })
      .css({ top: Math.max(0,minTop) })
      .addClass('resize');
  };

  // Focus the user to the expanded menu,
  // or return focus to the main part of the page
  $.fn.setFocus = function(options) {
    var section = $(this),
        choice = $('.choice', section),
        sections = m.focus
          .not(section)
          .not(options.c);
    if (options.focus) {
      if (options.hover) {
        section.hover(function() {}, function() {
          section.setFocus({ c: options.c, focus: false, hover: true });
          if (options.c.attr('id') == 'characters') {
            choice.html(choice.data('prev'));
          }
        });
      }
      section.addClass('expanded');
      m.body.addClass('focussed');
      sections.addClass('inactive');
    } else {
      section.unbind('hover').removeClass('expanded');
      m.body.removeClass('focussed');
      sections.removeClass('inactive');
    }
    return section;
  };

  // Set html to a webfont icon followed by a string/markup
  $.fn.htmlWebfont = function(letter, s) {
    var str = '<span aria-hidden="true" data-icon="'+letter+'"></span>';
    str = str + s;
    return $(this).html(str);
  };

  // Setup the overview section of the site showing characters and their assigned
  // compass settings
  $.fn.clickOverview = function() {
    var self = $(this);
    self
      .click(function() {
        m.sfx('cards');
        self.unbind('click');
        $('a.close', self)
          .unbind('click')
          .click(function() {
            self.setFocus({ c: m.overview, focus: false, hover: false });
            setTimeout(function() { m.overview.clickOverview(); }, 1000);
          });
        $('a.finished', self)
          .unbind('click')
          .click(function() {
            m.introOutroVideo(m.outro);
            m.outro.setFocus({ c: m.outro, focus: true, hover: false });
            m.stopAmbience();
          });
        setTimeout(function() {
          self.setFocus({ c: m.overview, focus: true, hover: false });
        }, 250);
      });
    return self;
  };

  // Expand the menu to display the choice of characters
  $.fn.clickCharacters = function() {
    var section = $(this)
        scenes = $('> div', m.scenes),
        choice = $('div.choice', section);
    choice
      .unbind('click')
      .click(function(e) {
        e.preventDefault();
        if (section.hasClass('expanded')) {
          choice.html(choice.data('prev'));
          section.setFocus({ c: m.characters, focus: false, hover: true });
        } else {
          choice.data('prev', choice.html());
          section.setFocus({ c: m.characters, focus: true, hover: true });
        }
      });
    return section;
  };

  // Click the active scene set to expand the list of scenes
  $.fn.clickScenes = function() {
    var section = $(this)
        scenes = $('> div', m.scenes),
        choice = $('div.choice', section);
    section
      .unbind('click')
      .click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('active') && !m.body.hasClass('focussed') && $(this).hasClass('visible')) {
          $(this).setFocus({ c: m.scenes, focus: true, hover: true });
        }
      });
    return $(this);
  };

  // Click action when selecting a character
  $.fn.characterSelect = function(key) {
    var self = $(this);
    self
      .addClass('active')
      .siblings()
      .removeClass('active')
      .filter('.choice')
      .html(self.html())
      .data('prev',self.html())
      .removeClass('complete')
      .parent()
      .setFocus({ c: m.characters, focus: false, hover: true })
      .clickCharacters();
    m.selected.character = key;
    m.scenes.addClass('visible');
    $('#scenes-'+key)
      .siblings()
      .removeClass('active');
    $('#scenes-'+key)
      .displaySegments(key)
      .addClass('active')
    return self;
  };

  // Function that runs when a scene is selected
  // The site displays a scene information page along with accompaying
  // activity pages along with a navigation menu and close button
  $.fn.sceneSelect = function() {
    var sceneObj = $(this),
        id = '#'+sceneObj.attr('id')+'-player',
        activity, arr = {};
    // Makes sure a scene has been clicked on an expanded scene menu
    // This stops accidental scene clicking when the menu is contracted
    if (m.scenes.filter('.expanded').length > 0) {
      m.loadingActive(true);
      arr = { id: sceneObj.attr('data-key'), video: null, title: null, activities: [] };
      arr.video = m.settings.scenes[arr.id].video;
      arr.title = m.settings.scenes[arr.id].title;
      for (var i = 1; i <= 3; i++) {
        var act = m.settings.activities[arr.id+'_'+i];
        arr.activities.push({
          title: arr.title,
          subtitle: act.title,
          image: act.image,
          description: act.description,
          id: arr.id+'_'+i
        });
      }
      // After loading animation appears (1s fadein time), run changes to the DOM
      setTimeout(function() {
        m.main.hide();
        // Add scene information/activity pages
        m.body.append(m.templates.activity(arr));
        activity = $('> div.overlay', m.body);
        // Adjust all pages so they are vertically centered
        $('> div.scene-view-wrapper > div', activity).verticalCentre();
        // Initialise the video player on the scene information page
        var vid = $('video', activity).mediaelementplayer();
        vid.on('play', function () {
          m.stopAmbience();
        }).on('pause ended', function () {
          m.startAmbience();
        });
        activity.initNavButtons();
        // Remove the loading overlay when video is ready
        m.loadingActive(false);
      }, 1000);
    }
  };

  // Load all scenes that have been already placed onto the compass
  $.fn.displaySegments = function(character) {
    var self = $(this), completed_scenes, scene;
    // Remove all scenes from current segment
    $('div.segment', m.segments).each(function() {
      var segment = $(this);
      $('div.scene', segment).each(function() {
        segment.removeScene($(this));
      });
    });
    // Add all pre-placed scenes to segments
    completed_scenes = $('.scene.complete', self).each(function() {
      scene = $(this).attr('data-key');
      seg = m.settings.segments.indexOf(m.cookie[character][scene]);
      if (seg > -1) {
        var segment = $('.segment-'+seg, m.segments);
        m.selected.segment = seg;
        segment.addScene($(this).createDragObj(m.body, null));
      }
    });
    if (completed_scenes.length == 3) {
      $('div.choice,#character-'+character, m.characters).addClass('complete');
    }
    return self;
  };

  // Add click events to the navigation menu of the scene information and activity
  // pages, and to the close button
  // Certain click events stop projekktor from playing
  // and destroys video player to free up memory
  $.fn.initNavButtons = function() {
    var self = $(this),
        links = $('nav a', this),
        pages = $('div.scene-view-wrapper > div', this);
    links.each(function(i) {
      var link = $(this);
      if (links.length - 1 > i) {
        link
          .unbind('click')
          .click(function(e) {
            e.preventDefault();
            link.changeActivity(pages);
          });
      } else {
        link.activitiesBack();
      }
    });
    $('footer a.back', pages).each(function() {
      $(this).activitiesBack();
    });
    return self;
  };

  // Function to change the current activity page
  $.fn.changeActivity = function(pages) {
    var self = $(this),
        cur = pages.filter('current').removeClass('current').index(),
        choice = self.index();
    pages.removeClass('left current');
    for (var i = choice - 1; i >= 0; i-=1) {
      pages.eq(i).addClass('left');
    }
    pages.eq(choice).addClass('current');
    self.addClass('active').siblings().removeClass('active');
    return self;
  };

  // Go back to main compass page from activities
  $.fn.activitiesBack = function() {
    $(this).click(function(e) {
      e.preventDefault();
      m.loadingActive(true);
      m.startAmbience();
      setTimeout(function() {
        m.main.show();
        $('.scene-view-wrapper video')[0].pause();
        $('> div.overlay', m.body).remove();
        m.loadingActive(false);
      }, 1000);
    });
    return $(this);
  };

  // Once a scene is dropped in a compass segment, the scene object is added to
  // the compass and removed from under the cursor.
  // The linked scene from the menu is also marked as complete
  // and its parent character is marked as complete if all scenes are completed
  $.fn.addScene = function(drag) {
    var segment = $(this),
        character_id = m.selected.character,
        charChoice = $('div.choice,#character-'+character_id, m.characters),
        scene_id = drag.data('settingId'),
        attr_id = '#activity-' + scene_id,
        s = $(attr_id), ss, scene, remove,
        seg_choice = m.settings.segments[m.selected.segment];
    m.cookieVal(character_id, scene_id, seg_choice);
    m.updateProgress();
    m.updateOverview({
      character: character_id, 
      scene: scene_id, 
      segment: seg_choice,
      add: true
    });
    console.log(drag[0]);
    drag
      .clone()
      .removeAttr('style')
      .addClass('position')
      .data('id', scene_id)
      .appendTo(segment);
    $('.active', m.compass).addClass('fade-out').removeClass('active');
    setTimeout(function() { $('.fade-out', m.compass).removeClass('fade-out'); }, 4000);
    scene = segment.recountScenes().children().last();
    s.addClass('complete');
    ss = $('#scenes-'+character_id+' div.scene.complete');
    if (ss.length == 3) {
      charChoice.addClass('complete');
    }
    remove = $('<div>')
      .htmlWebfont('d', 'Remove')
      .addClass('remove')
      .click(function() {
        s.removeClass('complete');
        charChoice.removeClass('complete');
        m.cookieVal(character_id, scene_id, '');
        console.log(character_id, scene_id);
        m.updateProgress();
        m.updateOverview({
          character: character_id, 
          scene: scene_id, 
          segment: seg_choice,
          add: false
        });
        segment.removeScene(scene);
      });
    scene
      .append(remove)
      .find('img')
      .removeAttr('draggable ondragstart');
    setTimeout(function() {
      scene.removeClass('position');
    }, 100);
    return segment;
  };

  // Add video markup to a container
  $.fn.addVideo = function(vid, id) {
    var video = $('<video>')
          .attr({
            poster: 'video/'+vid+'.jpg',
            id: id,
            width: 624,
            height: 352,
            autoplay: false
            // src: 'video/'+vid+'.mp4'
          });
    $('<source />')
      .attr({
        src: 'video/'+vid+'.mp4',
        type: 'video/mp4'
      })
      .appendTo(video);
    $('<source />')
      .attr({
        src: 'video/'+vid+'.webm',
        type: 'video/webm'
      })
      .appendTo(video);
    $('<source />')
      .attr({
        src: 'video/'+vid+'.ogv',
        type: 'video/ogg'
      })
      .appendTo(video);
    return $(this).append(video);
  };


  // This action is run when the user clicks the remove button in a scene
  // that has been assigned to a compass segment
  $.fn.removeScene = function(scene) {
    var segment = $(this);
    scene.addClass('position');
    setTimeout(function() {
      scene.remove();
      segment.recountScenes();
    }, 1000);
    return segment;
  };

  // Once a scene is dropped into a segment, the segment is counted to see how
  // mny scenes have been added there - a relevant class is added to elegantly
  // space out multiple scenes in a segment
  $.fn.recountScenes = function() {
    var self = $(this);
    return self
      .removeClass('items-0 items-1 items-2 items-3')
      .addClass('items-'+self.children().length)
      .children('div.scene')
      .each(function(i) {
        $(this)
          .removeClass('drag scene-1 scene-2 scene-3')
          .addClass('scene-'+(i+1));
        })
      .end();
  };

  // A drag object is created when a scene is dragged from its expanded menu.
  // The object takes on its original scene item's properties and identity
  $.fn.createDragObj = function(body, e) {
    var orig = $(this),
        newObj = orig
          .clone()
          .data('id', orig.attr('id'))
          .data('settingId', orig.attr('data-key'))
          .removeAttr('id')
          .addClass('drag');
    if (e) {
      newObj.css({
        top: e.pageY - 28,
        left: e.pageX - 50
      }).appendTo(body);
      return $('> .drag', body);
    }
    return newObj;
  }

  // Compass sectors are highlighted by assigning a 'selected' class to the
  // rollover-ed segment
  $.fn.highlightSector = function(x, y) {
    var self = $(this),
        c = m.dims, b, seg;
    if (x >= c.xMin && x < c.xMax && y >= c.yMin && y < c.yMax) {
      b = m.getBearing(c.cX, c.cY, x, y);
      seg = Math.floor(b / 45) % 8;
      if (seg != m.selected.segment) {
        self
          .removeClass('selection-'+m.selected.segment)
          .addClass('selection-'+seg);
        m.selected.segment = seg;
        $('> .compass-'+seg, m.compass).addClass('active').siblings().removeClass('active');
      }
    } else {
      self.removeClass('selection-'+m.selected.segment);
      m.selected.segment = -1;
      $('> .active', m.compass).addClass('fade-out').removeClass('active fade-out');
    }
    return self;
  };


  // Usage: $(['img1.jpg','img2.jpg']).preloadImages(function(){ ... });
  // Callback function gets called after all images are preloaded
  $.fn.preloadImages = function(callback) {
    var checklist = this.toArray(), i;
    this.each(function() {
      var src = this.toString();
      m.loadingMessage('Preloading '+this);
      $('<img>').attr({ src: './'+src }).load(function(response, status, xhr) {
        for (i = 0; i < checklist.length; i++) {
          if (checklist[i] === src) {
            checklist.splice(i,1);
            break;
          }
        }
        if (checklist.length == 0) {
          m.loadingMessage('Ready');
          callback();
        }
      });
    });
  };
  
  return m;

})(jQuery, soundManager, this, this.document);
